// Теоретичні питання

// Опишіть своїми словами як працює метод forEach.  
// Метод forEach дозволяє перебрати всі елементи в масиві, а також запустити функцію для кожного 
// елементу масиву. 
// Як очистити масив?
// Очистити масив можна за допомогою власивості length. Наприклад, arr.length = 0.
// Як можна перевірити, що та чи інша змінна є масивом?
// За допомогою метода Array.isArray(). Якщо змінна є масивом, метод поверне true, інакше поверне false.

"use strict";
function filterBy(arr, a) {
    let filtered = arr.filter(item => (typeof item !== typeof a));
    return filtered;
}

let arr = ['hello', 'world', 23, '23', null];
let filtered = filterBy(arr, 'string');

// let arr = [23, 15, null, 'vasja', 10];
// let filtered = filterBy(arr, 5);

// let arr = ['hi', 8, null, true];
// let filtered = filterBy(arr, false);

console.log(filtered);